You are asked to create a simple NodeJS app that lists all the projects each developer is working on. Everyone on your team wants to be able to see the list themselves and share with the project managers, so they ask you to make it available online, so everyone can access it.

EXERCISE 0: Clone Git Repository

EXERCISE 1: Package NodeJS App

EXERCISE 2: Create a new server
Your company uses DigitalOcean as Infrastructure as a Service platform, instead of having on-premise servers. So you:
Create a new droplet server on DigitalOcean

EXERCISE 3: Prepare server to run Node App

EXERCISE 4: Copy App and package.json

EXERCISE 5: Run Node App
Start the node application in detached mode (npm install and node server.js commands)

EXERCISE 6: Access from browser - configure firewall
You see that the application is not accessible through the browser, because all ports are closed on the server.
