<details>
<summary>Exercise 0: Clone project and create own Git repository </summary>
 <br />

```sh
# clone repository & change into project dir
git clone git@gitlab.com:twn-devops-bootcamp/latest/05-cloud/cloud-basics-exercises.git
cd clouexercise

# remove remote reference and add your project repository
rm -rf .git
git init 
git add .
git commit -m "Initial commit"
git remote add origin 
git push -u origin master

```
</details>

******
<details>
<summary>Exercise 1: Package NodeJS App  </summary>
 <br />

```sh
#npm pack command creates a tar file.
cd app 
npm pack

```

</details>



******
<details>
<summary>Exercise 2: Create a new server  </summary>
 <br />

```sh
#create a digital ocean accounr, create a new droplet

```

</details>




******
<details>
<summary>Exercise 3: Prepare server to run Node App </summary>
 <br />

```sh
#install nodejs and npm
ssh@root{server-ip-addess}
apt update
apt install nodejs 
apt install npm

#verify the installation
nodejs -v
npm -v

```

</details>

******


<details>
<summary>Exercise 4: Copy App and package.json  </summary>
 <br />

```sh
#create a folder for node on the server, scp command copies the file from local to remote server
scp bootcamp-node-project-1.0.0.tgz root@{server-ip-addess}:/root/node_my_app
scp package.json root@{server-ip-addess}:/root/node_my_app
```

</details>

******


<details>
<summary>Exercise 5: Run Node App  </summary>
 <br />

```sh
#ssh into the server and untar the tar file.
ssh root@{server-ip-addess}
tar -xzvf bootcamp-node-project-1.0.0.tgz

#cd into package folder
cd package

#install dependencies
npm install

#run the app
node server.js

```

</details>

******


<details>
<summary> Exercise 6: Access from browser - configure firewall
</summary>
</br>

```sh
#create a firewall rule in the droplet to open port 3000 for the app(custom rule)

```
</details>



